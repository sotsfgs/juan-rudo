
extends RigidBody2D

export var VEL=95
var DIRECCION=Vector2()
var N=Vector2(0, -1)
var S=Vector2(0, 1)
var E=Vector2(1, 0)
var O=Vector2(-1, 0)

var ORIENTACION="detenido_abajo_b"

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	set_angular_velocity(0)
	set_linear_velocity( Vector2(0,0) )
	_move(delta)
	
func _move(delta):
	var pos_act = get_pos()
	
	if Input.is_action_pressed("heroe_N"):
		DIRECCION = N
		ORIENTACION = "caminar_arriba_b"
	elif Input.is_action_pressed("heroe_S"):
		DIRECCION = S
		ORIENTACION = "caminar_abajo_b"
	elif Input.is_action_pressed("heroe_E"):
		DIRECCION = E
		ORIENTACION = "caminar_derecha_b"
	elif Input.is_action_pressed("heroe_O"):
		DIRECCION = O
		ORIENTACION = "caminar_izquierda_b"
	else:
		DIRECCION = Vector2()
		if ORIENTACION == "caminar_arriba_b":
			ORIENTACION = "detenido_arriba_b"
		elif ORIENTACION == "caminar_abajo_b":
			ORIENTACION = "detenido_abajo_b"
		elif ORIENTACION == "caminar_derecha_b":
			ORIENTACION = "detenido_derecha_b"
		elif ORIENTACION == "caminar_izquierda_b":
			ORIENTACION = "detenido_izquierda_b"
			
	# hacemos los calculos para la nueva posicion
	set_linear_velocity(  DIRECCION*VEL )
	var pos_nueva = pos_act + DIRECCION * VEL * delta
	
	var animacion_actual = get_node("Sprite").get_node("animacion").get_current_animation()
	if animacion_actual != ORIENTACION:
		get_node("Sprite").get_node("animacion").play(ORIENTACION)