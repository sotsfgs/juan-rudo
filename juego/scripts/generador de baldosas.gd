tool

func _run():
	var nodopadre = get_scene()
	var res = "res://datos/baldosas/mi atlas.png"
	if nodopadre:
		var ancho = 512
		var alto = 192
		var size = Vector2(32, 32)
		var nodoconteo = 0
		for x in range(0,ancho, 32):
			for y in range(0, alto, 32):
				print(nodoconteo, " ", x, " ", y)
				var xy = Vector2(x, y)
				var rect = Rect2(xy, size)
				var sprite = Sprite.new()
				sprite.set_name(str(nodoconteo))
				sprite.set_texture(load(res))
				sprite.set_region(true)
				sprite.set_region_rect(rect)
				sprite.set_pos(xy)
				nodopadre.add_child(sprite)
				sprite.set_owner(nodopadre)
				nodoconteo += 1